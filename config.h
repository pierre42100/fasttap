/**
 * Project configuration
 *
 * @author Pierre HUBERT
 */

// Initial level of a new game, should be always to 1
#define INITIAL_LEVEL 1

// Maximum time of a game, in seconds
#define MAX_TIME 60

// Tick interval
#define TICK_INTERVAL 50

// Language file name
#define LANG_NAME "fr"

// Initial speed
#define INITIAL_SPEED 100


// Compute score
#define SCORE_PER_SECS 40
#define WIN_PER_SUCCESS 100
#define LOOSE_PER_ERROR 200


// New lifes
#define NEW_LIFE_AFTER 100000
