#ifndef CURRENTSTATS_H
#define CURRENTSTATS_H

#include <QString>

#include "config.h"

/**
 * @author Pierre HUBERT
 */
class CurrentStats
{
public:
    CurrentStats();

    int numberWords() const;
    void addWord();
    void setNumberWords(int numberWords);

    int numberErrors() const;
    void addError();
    void setNumberErrors(int numberErrors);

    int numberKeyStrokes() const;
    void addKeyStroke();
    void setNumberKeyStrokes(int numberKeyStrokes);

    int numbersLives() const;
    void addLife();
    void removeLife();
    void setNumbersLives(int numbersLives);

    int currentLevel() const;
    void increaseLevel();
    void setCurrentLevel(int currentLevel);

    int currentSpeed() const;

    int currScore() const;
    void addToScore(int value);
    void removeFromScore(int value);
    void setCurrScore(int currScore);

    int earnedLifes() const;

    int percentSuccess() const;

private:
    int mNumberWords = 0;
    int mNumberErrors = 0;
    int mNumberKeyStrokes = 0;
    int mNumbersLives = 3;
    int mCurrentLevel = INITIAL_LEVEL;
    int mCurrScore = 0;
    int mEarnedLifes = 0;
};

#endif // CURRENTSTATS_H
