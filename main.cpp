#include <QApplication>
#include <mainwidget.cpp>
#include "time.h"

int main(int argc, char **argv){

    srand(time(NULL));

	QApplication app(argc, argv);

	MainWidget w;
	w.show();

	return app.exec();

}
