#ifndef GAMEOVERWIDGET_H
#define GAMEOVERWIDGET_H

#include <QDialog>

#include "currentstats.h"

namespace Ui {
class GameOverWidget;
}

class GameOverWidget : public QDialog
{
    Q_OBJECT

public:
    explicit GameOverWidget(const CurrentStats &stats, QWidget *parent = nullptr);
    ~GameOverWidget();

private:
    Ui::GameOverWidget *ui;
    CurrentStats mStats;
};

#endif // GAMEOVERWIDGET_H
