QT += widgets


SOURCES += \
	main.cpp \
	wordslist.cpp \
    mainwidget.cpp \
    currentstats.cpp \
    gameoverwidget.cpp


HEADERS += \
	config.h \
	wordslist.h \
    mainwidget.h \
    currentstats.h \
    gameoverwidget.h

FORMS += \
    mainwidget.ui \
    gameoverwidget.ui

RESOURCES += \
    ressources.qrc