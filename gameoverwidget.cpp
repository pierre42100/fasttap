#include "gameoverwidget.h"
#include "ui_gameoverwidget.h"

GameOverWidget::GameOverWidget(const CurrentStats &stats, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameOverWidget),
    mStats(stats)
{
    ui->setupUi(this);

    QString message = tr(
                "You have lost!\n"
                "You have typed %1 words with a total of %2 letters.\n"
                "You have done %3 errors, that gives you a success rate of %4%\n"
                "You have finished with a total score of %5 points.")
            .arg(mStats.numberWords())
            .arg(mStats.numberKeyStrokes())
            .arg(mStats.numberErrors())
            .arg(mStats.percentSuccess())
            .arg(mStats.currScore());
    ui->message->setText(message);
}

GameOverWidget::~GameOverWidget()
{
    delete ui;
}
