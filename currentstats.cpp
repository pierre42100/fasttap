#include "currentstats.h"
#include "config.h"

CurrentStats::CurrentStats()
{

}

int CurrentStats::numberWords() const
{
    return mNumberWords;
}

void CurrentStats::addWord()
{
    mNumberWords++;
}

void CurrentStats::setNumberWords(int numberWords)
{
    mNumberWords = numberWords;
}

int CurrentStats::numberErrors() const
{
    return mNumberErrors;
}

void CurrentStats::addError()
{
    mNumberErrors++;
}

void CurrentStats::setNumberErrors(int numberErrors)
{
    mNumberErrors = numberErrors;
}

int CurrentStats::numberKeyStrokes() const
{
    return mNumberKeyStrokes;
}

void CurrentStats::addKeyStroke()
{
    mNumberKeyStrokes++;
}

void CurrentStats::setNumberKeyStrokes(int numberKeyStrokes)
{
    mNumberKeyStrokes = numberKeyStrokes;
}

int CurrentStats::numbersLives() const
{
    return mNumbersLives;
}

void CurrentStats::addLife()
{
    mNumbersLives++;
}

void CurrentStats::removeLife()
{
    mNumbersLives--;
}

void CurrentStats::setNumbersLives(int numbersLives)
{
    mNumbersLives = numbersLives;
}

int CurrentStats::currentLevel() const
{
    return mCurrentLevel;
}

void CurrentStats::increaseLevel()
{
    mCurrentLevel++;
}

void CurrentStats::setCurrentLevel(int currentLevel)
{
    mCurrentLevel = currentLevel;
}

int CurrentStats::currentSpeed() const
{
    return currentLevel() * INITIAL_SPEED;
}

int CurrentStats::currScore() const
{
    return mCurrScore;
}

void CurrentStats::addToScore(int value)
{
    mCurrScore += value;

    if(mCurrScore / NEW_LIFE_AFTER > mEarnedLifes){
        addLife();
        mEarnedLifes++;
    }
}

void CurrentStats::removeFromScore(int value)
{
    mCurrScore -= value;
}

void CurrentStats::setCurrScore(int currScore)
{
    mCurrScore = currScore;
}

int CurrentStats::earnedLifes() const
{
    return mEarnedLifes;
}

int CurrentStats::percentSuccess() const
{
    return 100 - (numberErrors()*100 / numberKeyStrokes());
}
