#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QMainWindow>

#include "currentstats.h"


class QTimer;
class QLineEdit;

class WordsList;

namespace Ui {
class MainWidget;
}

enum GameStatus {
    STOPPED,
    STARTED,
    PAUSED
};

class MainWidget : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = nullptr);
    ~MainWidget() override;
    GameStatus getStatus();
    int userInput();
    QString currentWord();

    void setMessage(const QString &msg);

private slots:
    void startGame();
    void tickTime();
    void changeWord();
    void resumeGame();
    void pauseGame();
    void togglePause();
    void stopGame();
    void goNextLevel();
    void newText();
    void blinkScreen(Qt::GlobalColor color);
    void setBackgroundColor(Qt::GlobalColor color);
    void resetBackground();

    void on_actionNew_triggered();

    void on_actionPause_triggered();

    void on_actionStop_triggered();

    void on_actionQuit_triggered();

    void on_actionAbout_triggered();

private:
    void refreshUI();
    void initGame();
    void startTimer();
    void stopTimer();

    //Private membres
    Ui::MainWidget *ui;
    WordsList *mWordsList;
    GameStatus mStatus = STOPPED;
    int mRemainingTime = 0;
    int mCurrentInput = 0;
    int mWordAgeMs = 0;
    CurrentStats mCurrentStats;
    QTimer *mTimer;
    int mTimerInterval = 0;
    QString mCurrentWord;
    QLineEdit *mTextInput;
};

#endif // MAINWIDGET_H
