/**
 * Words list managment
 *
 * @author Pierre HUBERT
 */

#include <QObject>
#include <QStringList>

class WordsList {

public:
	WordsList(QString lang);

    QStringList words() const;

    QString random() const;

private:
    QStringList mWords;
};
