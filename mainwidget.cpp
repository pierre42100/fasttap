#include <QPushButton>
#include <QTimer>
#include <QMessageBox>
#include <QKeyEvent>
#include <QLineEdit>
#include <QFontDatabase>

#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "wordslist.h"
#include "config.h"
#include "gameoverwidget.h"

MainWidget::MainWidget(QWidget *parent) :
    QMainWindow (parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);
    setFixedSize(width(), height());

    //Fix font
    int fontID = QFontDatabase::addApplicationFont(":/fonts/Arimo-Bold.ttf");
    QString family = QFontDatabase::applicationFontFamilies(fontID).at(0);
    QFont font(family);
    font.setPointSize(23);
    ui->word->setFont(font);


    mWordsList = new WordsList(LANG_NAME);

    //Create focus grabber
    mTextInput = new QLineEdit(this);
    mTextInput->move(-100, -100);
    connect(mTextInput, &QLineEdit::textChanged, this, &MainWidget::newText);
    setFocusProxy(mTextInput);
    setFocusPolicy(Qt::StrongFocus);

    refreshUI();

    setMessage(tr("Press F2 to start a new game"));
}

MainWidget::~MainWidget()
{
    delete ui;
}

GameStatus MainWidget::getStatus()
{
    return mStatus;
}

int MainWidget::userInput()
{
    return mCurrentInput;
}

QString MainWidget::currentWord()
{
    return mCurrentWord.toUpper();
}

void MainWidget::setMessage(const QString &msg)
{
    ui->message->setText(msg);
}

void MainWidget::newText()
{
    if(getStatus() == STOPPED) return;
    if(mTextInput->text().length() == 0) return;

    //Get character
    QChar c = mTextInput->text().at(0).toUpper();
    mTextInput->setText("");

    //Toggle pause if required
    if(c == ' '){
        togglePause();
        return;
    }

    //Continue only if game is started
    if(getStatus() != STARTED)
        return;

    mCurrentStats.addKeyStroke();

    if(currentWord().at(userInput()) == c){
        mCurrentInput++;
        mCurrentStats.addToScore(WIN_PER_SUCCESS);
        blinkScreen(Qt::green);
        setMessage(tr("Continue!"));
    }
    else {
        blinkScreen(Qt::red);
        mCurrentStats.removeFromScore(LOOSE_PER_ERROR);
        mCurrentStats.addError();
        setMessage(tr("You must type the letter '%1'!").arg(currentWord().at(userInput())));
    }

    if(currentWord().length() == userInput()){
        mCurrentStats.addWord();
        changeWord();
        setMessage(tr("Good job!"));
    }

    refreshUI();

}

void MainWidget::startGame()
{
    //Start the game
    initGame();
    resumeGame();
    refreshUI();

    setMessage(tr("Let's go!!!"));
}

void MainWidget::tickTime()
{
    //Score
    mCurrentStats.addToScore(SCORE_PER_SECS/(1000/TICK_INTERVAL));

    //Countdown timer
    mTimerInterval++;
    if(mTimerInterval == 1000/TICK_INTERVAL){
        mRemainingTime--;
        refreshUI();

        if(mRemainingTime == 0)
            goNextLevel();

        mTimerInterval = 0;
    }


    //Move word
    //Height
    int y = (ui->wordContainer->height() - ui->word->height())/2;

    //Width
    int x = ui->word->x() - (mCurrentStats.currentSpeed() * (TICK_INTERVAL / 2500.0));

    ui->word->move(x, y);
    mWordAgeMs += 50;

    //Check for word timeout
    if(x < -1 * ui->word->width()) {
        changeWord();
        mCurrentStats.removeLife();
        blinkScreen(Qt::red);
        setMessage(tr("Missed!"));

        if(mCurrentStats.numbersLives() < 0)
            stopGame();
    }

    refreshUI();
}

void MainWidget::changeWord()
{
    mCurrentWord = mWordsList->random();
    mCurrentInput = 0;
    mWordAgeMs = 0;

    ui->word->setText(mCurrentWord.toUpper());
    ui->word->setFixedWidth(ui->word->fontMetrics().width(ui->word->text()) + 5);
    ui->word->move(ui->wordContainer->width() + 1, ui->wordContainer->height());
}

void MainWidget::refreshUI()
{
    //Refresh buttons
    ui->actionPause->setEnabled(mStatus != STOPPED);
    ui->actionPause->setChecked(mStatus == PAUSED);
    ui->actionStop->setEnabled(mStatus != STOPPED);

    //Refresh stats
    ui->timeLabel->setText(QString::number(mRemainingTime));
    ui->lifes->setText(QString::number(mCurrentStats.numbersLives()));
    ui->level->setText(QString::number(mCurrentStats.currentLevel()));
    ui->score->setText(QString::number(mCurrentStats.currScore()));

    //Process word
    QString word;
    word.append("<span style=\" color: rgb(78, 154, 6);;\">");
    for(int i = 0; i < currentWord().length(); i++){


        if(i == userInput())
            word.append("</span>");


        word.append(currentWord().at(i));




    }


    ui->word->setText(word.toLower());
}

void MainWidget::initGame()
{
    mRemainingTime = MAX_TIME;
    mCurrentStats = CurrentStats();

    changeWord();
}

void MainWidget::startTimer()
{
    mTimer = new QTimer();
    mTimer->setInterval(TICK_INTERVAL);
    mTimer->start();
    connect(mTimer, &QTimer::timeout, this, &MainWidget::tickTime);
}

void MainWidget::stopTimer()
{
    mTimer->stop();
    mTimer->deleteLater();
}

void MainWidget::blinkScreen(Qt::GlobalColor color)
{
    setBackgroundColor(color);

    QTimer::singleShot(100, this, &MainWidget::resetBackground);
}

void MainWidget::setBackgroundColor(Qt::GlobalColor color)
{
    setAutoFillBackground(true);
    setPalette(color);
}

void MainWidget::resetBackground()
{
    setAutoFillBackground(false);
    setPalette(QPalette());
}

void MainWidget::resumeGame()
{
    mStatus = STARTED;

    startTimer();

    refreshUI();
    mTextInput->setFocus();

    //Ensure there is no background color to the word area
    resetBackground();
    ui->word->setVisible(true);
}

void MainWidget::pauseGame()
{
    if(getStatus() != STARTED)
        return;

    stopTimer();
    mStatus = PAUSED;
    refreshUI();

    //Change word target area background
    setBackgroundColor(Qt::gray);
    ui->word->setVisible(false);
}

void MainWidget::togglePause()
{
    if(getStatus() == PAUSED)
        resumeGame();
    else
        pauseGame();
}

void MainWidget::stopGame()
{
    if(mStatus == STARTED)
        pauseGame();

    GameOverWidget(mCurrentStats, this).exec();

    mStatus = STOPPED;
    mRemainingTime = 0;
    mCurrentStats = CurrentStats();
    mCurrentWord = "";

    refreshUI();
}

void MainWidget::goNextLevel()
{
    stopTimer();

    mCurrentStats.increaseLevel();
    QMessageBox::information(this, tr("Level"), tr("Good job, you reached level %1 !").arg(mCurrentStats.currentLevel()));

    //Reset word and remaining time
    mRemainingTime = MAX_TIME;
    changeWord();

    startTimer();
}

void MainWidget::on_actionNew_triggered()
{
    startGame();
}

void MainWidget::on_actionPause_triggered()
{
    togglePause();
}

void MainWidget::on_actionStop_triggered()
{
    stopGame();
}

void MainWidget::on_actionQuit_triggered()
{
    QCoreApplication::quit();
}

void MainWidget::on_actionAbout_triggered()
{
    QMessageBox::information(this, tr("About"), tr("FastTap is an OpenSource imitation of the old TipTap software."));
}
