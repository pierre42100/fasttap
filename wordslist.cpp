#include <QFile>

#include "wordslist.h"


WordsList::WordsList(QString lang) {

    //Open matching file
    QFile file(":/lang/"+lang+".txt");
    if(!file.open(QFile::ReadOnly))
        qFatal("Could not open lang file!");

    QByteArray data;
    while(!(data = file.readLine()).isEmpty()){
        mWords << data.replace("\n", "");

    }


    //At the end, close the file
    file.close();

}

QStringList WordsList::words() const
{
    return mWords;
}

QString WordsList::random() const
{
    return mWords.at(rand() % mWords.size());
}
